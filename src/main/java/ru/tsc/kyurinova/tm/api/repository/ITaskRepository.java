package ru.tsc.kyurinova.tm.api.repository;

import ru.tsc.kyurinova.tm.enumerated.Status;

import ru.tsc.kyurinova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    void add(String userId, Task task);

    void remove(String userId, Task task);

    List<Task> findAll(String userId);

    List<Task> findAll(String userId, Comparator<Task> comparator);

    void clear(String userId);

    Task findById(String userId, String id);

    Task findByName(String userId, String name);

    Task findByIndex(String userId, Integer index);

    Task removeById(String userId, String id);

    Task removeByName(String userId, String name);

    Task removeByIndex(String userId, Integer index);

    Task startById(String userId, String id);

    Task startByIndex(String userId, Integer index);

    Task startByName(String userId, String name);

    Task finishById(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

    Task changeStatusById(String userId, String id, Status status);

    Task changeStatusByIndex(String userId, Integer index, Status status);

    Task changeStatusByName(String userId, String name, Status status);

    Task bindTaskToProjectById(String userId, String projectId, String taskId);

    Task unbindTaskToProjectById(String userId, String projectId, String taskId);

    Task findByProjectAndTaskId(String userId, String projectId, String taskId);

    boolean existsById(String userId, String id);

    boolean existsByIndex(String userId, Integer index);

    void removeAllTaskByProjectId(String userId, String projectId);

    List<Task> findAllTaskByProjectId(String userId, String projectId);
}
