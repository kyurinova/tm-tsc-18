package ru.tsc.kyurinova.tm.api.repository;

import ru.tsc.kyurinova.tm.enumerated.Status;

import ru.tsc.kyurinova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    boolean existsById(String userId, String id);

    boolean existsByIndex(String userId, Integer index);

    void add(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findAll(String userId);

    List<Project> findAll(String userId, Comparator<Project> comparator);

    void clear(String userId);

    Project findById(String userId, String id);

    Project findByName(String userId, String name);

    Project findByIndex(String userId, Integer index);

    Project removeById(String userId, String id);

    Project removeByName(String userId, String name);

    Project removeByIndex(String userId, Integer index);

    Project startById(String userId, String id);

    Project startByIndex(String userId, Integer index);

    Project startByName(String userId, String name);

    Project finishById(String userId, String id);

    Project finishByIndex(String userId, Integer index);

    Project finishByName(String userId, String name);

    Project changeStatusById(String userId, String id, Status status);

    Project changeStatusByIndex(String userId, Integer index, Status status);

    Project changeStatusByName(String userId, String name, Status status);

}
