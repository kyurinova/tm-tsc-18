package ru.tsc.kyurinova.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
