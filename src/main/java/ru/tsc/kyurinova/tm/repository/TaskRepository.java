package ru.tsc.kyurinova.tm.repository;

import ru.tsc.kyurinova.tm.api.repository.ITaskRepository;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, final Task task) {
        if (userId.equals(task.getUserId())) {
            tasks.add(task);
        }
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (!userId.equals(task.getUserId()))
            tasks.remove(task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (Task task : tasks) {
            if (userId.equals(task.getUserId()))
                result.add(task);
        }
        return result;
    }

    @Override
    public List<Task> findAll(final String userId, Comparator<Task> comparator) {
        final List<Task> listOfTasks = new ArrayList<>();
        for (Task task : tasks) {
            if (userId.equals(task.getUserId()))
                listOfTasks.add(task);
        }
        listOfTasks.sort(comparator);
        return listOfTasks;
    }

    @Override
    public void clear(final String userId) {
        final List<Task> listOfTasks = findAll(userId);
        this.tasks.removeAll(listOfTasks);
    }

    @Override
    public Task findById(final String userId, final String id) {
        for (Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByName(final String userId, final String name) {
        for (Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final String userId, final Integer index) {
        final List<Task> listOfTasks = findAll(userId);
        return listOfTasks.get(index);
    }

    @Override
    public Task removeById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        tasks.remove(task);
        return task;
    }

    @Override
    public Task startById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(final String userId, final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(final String userId, final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, final Integer index, final Status status) {
        final Task task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(final String userId, final String name, final Status status) {
        final Task task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        final Task task = findById(userId, id);
        return task != null;
    }

    @Override
    public boolean existsByIndex(final String userId, final Integer index) {
        final Task task = findByIndex(userId, index);
        return task != null;
    }

    @Override
    public Task bindTaskToProjectById(final String userId, final String projectId, final String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskToProjectById(final String userId, final String projectId, final String taskId) {
        final Task task = findByProjectAndTaskId(userId, projectId, taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public Task findByProjectAndTaskId(final String userId, final String projectId, final String taskId) {
        for (Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (taskId.equals(task.getId()) && projectId.equals(task.getProjectId())) return task;
        }
        return null;
    }

    @Override
    public void removeAllTaskByProjectId(final String userId, final String projectId) {
        List<Task> listByProject = findAllTaskByProjectId(userId, projectId);
        for (Task task : listByProject) {
            tasks.remove(task);
        }
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, final String projectId) {
        List<Task> listByProject = new ArrayList<>();
        for (Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (projectId.equals(task.getProjectId())) listByProject.add(task);
        }
        return listByProject;
    }
}
