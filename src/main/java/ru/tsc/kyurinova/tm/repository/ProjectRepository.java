package ru.tsc.kyurinova.tm.repository;

import ru.tsc.kyurinova.tm.api.repository.IProjectRepository;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final String userId, final Project project) {
        if (userId.equals(project.getUserId())) {
            projects.add(project);
        }
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (!userId.equals(project.getUserId()))
            projects.remove(project);
    }

    @Override
    public List<Project> findAll(final String userId) {
        final List<Project> result = new ArrayList<>();
        for (Project project : projects) {
            if (userId.equals(project.getUserId()))
                result.add(project);
        }
        return result;
    }

    @Override
    public List<Project> findAll(final String userId, Comparator<Project> comparator) {
        final List<Project> listOfProjects = new ArrayList<>(projects);
        for (Project project : projects) {
            if (userId.equals(project.getUserId()))
                listOfProjects.add(project);
        }
        listOfProjects.sort(comparator);
        return listOfProjects;
    }

    @Override
    public void clear(final String userId) {
        final List<Project> listOfProjects = findAll(userId);
        this.projects.removeAll(listOfProjects);
    }

    @Override
    public Project findById(final String userId, final String id) {
        for (Project project : projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByName(final String userId, final String name) {
        for (Project project : projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final String userId, final Integer index) {
        final List<Project> listOfProjects = findAll(userId);
        return listOfProjects.get(index);
    }

    @Override
    public Project removeById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        projects.remove(project);
        return project;
    }


    @Override
    public Project startById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByIndex(final String userId, final Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(final String userId, final Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project changeStatusById(final String userId, final String id, final Status status) {
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final String userId, final Integer index, final Status status) {
        final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(final String userId, final String name, final Status status) {
        final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        final Project project = findById(userId, id);
        return project != null;
    }

    @Override
    public boolean existsByIndex(final String userId, final Integer index) {
        final Project project = findByIndex(userId, index);
        return project != null;
    }
}
